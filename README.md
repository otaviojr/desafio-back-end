# API de filmes

## Instalação
##### 1. Clone o projeto
~~~
git clone https://gitlab.com/otaviojr/desafio-back-end.git
~~~
##### 2. Instale as dependências
~~~
npm install package.json 
~~~

## Configurar envio de e-mail
Para testar o envio de e-mail foi utilizado a ferramenta [Mailtrap](https://mailtrap.io/), deve-se, portanto, preencher o arquivo de configuração "_mail.js_" (./src/config/mail.js) com seus dados de autenticação, alterando as propriedades "_user_" e "_pass_".

~~~javascript
export default{
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "",
      pass: ""
    }
}    
~~~

## Iniciar servidor
Utilize o comando
~~~ 
npm run serve 
~~~ 

## Documentação

Para visualizar a documentação, acesse a rota __/docs__
