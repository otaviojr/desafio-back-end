import jwt from 'jsonwebtoken';
import authConfig from '@/config/authConfig';

export default (req, res, next) => {
    const authHeader = req.headers.authorization;

    if(authHeader){
        const tokenData = authHeader.split(' ');

        if(tokenData.length !== 2)
            res.status(400).send({error: 'Invalid token'});

        const [scheme, token] = tokenData;

        if(scheme.indexOf('Bearer') < 0)
            return res.status(400).send({error: 'No valid token provided'});

        try{
            const decoded = jwt.verify(token, authConfig.secret);
            
            req.uid = decoded.uid;
            next();
        }catch(e){
            console.error(e);
            res.status(400).send({error: 'Invalid token'});
        }

    }else 
        res.status(400).send({error: 'Invalid Token'});
};
