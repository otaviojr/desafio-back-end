import {Router} from 'express';
import Slugify from '@/utils/Slugify';
import Movie from '@/app/schemas/Movie';
import authMiddleware from '@/app/middlewares/Auth.js';

const router = new Router();

router.get('/', async(req, res) => {
    try{
        const moviesData = await Movie.find();
        
        const moviesList = moviesData.map(movie => {
            return {
                id: movie.id,
                title: movie.title,
                slug: movie.slug,
                description: movie.description,
                year: movie.year
            };
        });

        res.status(200).send(moviesList);
    }catch(e){
        console.error('Error', e);
        res.status(500).send('Internal server error');
    }
});

router.get('/:slug', async(req, res) => {
    const {slug} = req.params;

    try{
        const movieData = await Movie.findOne({slug});

        if(!movieData)
            res.status(404).send({error: 'Movie not found'});

        res.status(200).send(movieData);
    }catch(e){
        console.error('Error', e);
        res.status(500).send('Internal server error');
    }
});

router.post('/create', authMiddleware, async(req, res) => {
    const {title, description, year} = req.body;
    const slug = Slugify(title);

    try{
        let movieData = await Movie.findOne({slug});

        if(movieData)
            res.status(400).send({error: 'Movie already registered'});

        movieData = await Movie.create({title, slug, description, year});

        res.status(200).send(movieData);
    }catch(e){
        console.error('Error: ', e);
        res.status(500).send({error: 'Internal server error'});
    }
});

router.put('/update/:id', authMiddleware, async(req, res) => {
    const {id} = req.params;
    const {title, description, year} = req.body;
    let slug = (title) ? Slugify(title) : undefined;

    try{
        const movieUpdate = await Movie.findByIdAndUpdate(id, {
            title,
            slug, 
            description, 
            year
        }, {new: true});
            
        res.status(200).send(movieUpdate);
    }catch(e){
        console.error(e);
        res.status(500).send({error: 'Internal error server'});
    }
});

router.delete('/delete/:id', authMiddleware, async(req, res) => {
    const {id} = req.params;

    try{
        const movieRemove = await Movie.findByIdAndRemove(id);
        res.status(200).send(movieRemove);
    }catch(e){
        console.error(e);
        res.status(500).send({error: 'Internal error server'});
    }
});

export default router;
