import {Router} from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import Mailer from '@/modules/Mailer'
import User from '@/app/schemas/User';
import authConfig from '@/config/authConfig';

const router = new Router();

const generateToken = payload => {
    return jwt.sign(payload, authConfig.secret, {
        expiresIn: 86400 //1d
    });
};

router.post('/register', async(req, res) => { 
    const {name, email, password} = req.body;

    try{
        const userData = await User.findOne({email});

        if(userData)
            res.status(400).send({error: 'User already exists'});
        
        const newUser = await User.create({name, email, password});
        newUser.password = undefined;
        
        res.status(200).send(newUser);
    }catch(e){
        console.error('Error', e);
        res.status(500).send({error: 'Internal server error'});
    }
});

router.post('/login', async(req, res) => {
    const {email, password} = req.body;

    try{
        const userData = await User.findOne({email}).select('+password');

        if(!userData)
            res.status(404).send({error: 'User not found'});

        const result = await bcrypt.compare(password, userData.password);            
        
        if(!result)
            res.status(400).send({error: 'Invalid password'});
        
        const token = generateToken({uid: userData.id});

        res.status(200).send({
            token,
            tokenExpiration: '1d'
        });
    }catch(e){
        console.error('Error: ', e);
        res.status(500).send({error: 'Authentication failed'});
    }
});

router.post('/forgot-password', async(req, res) => {
    const {email} = req.body;

    try{
        const userData = await User.findOne({email});

        if(!userData)
            res.status(404).send({error: 'User not found'});

        const token = crypto.randomBytes(20).toString('hex');
        const expiration = new Date();
        expiration.setHours(new Date().getHours() + 3); //3 hours expiration;

        const result = await User.findByIdAndUpdate(userData.id, {
            $set: {
                passwordResetToken: token,
                passwordResetTokenExpiration: expiration
            }
        });

        Mailer.sendMail({
            to: email,
            from: 'verify@compjunior.com.br',
            template: 'auth/forgot_password',
            context: {token}
        }, error =>{
            if(error){
                console.error('Fail sending recover password mail', error);
                return res.status(400).send({error: 'Fail sending recover password mail'});
            }else{
                return res.status(200).send({message: 'Successful sending of email'});
            }
        });
    }catch(e){
        console.error('Error', e);
        res.status(500).send({error: 'Internal server error'});
    }
});

router.post('/reset-password', async(req, res) => {
    const {email, token, newPassword} = req.body;

    try{
        const userData = await User.findOne({email})
        .select('+passwordResetToken passwordResetTokenExpiration');
        
        if(!userData)
            res.status(404).send({error: 'User not found'});
        
        if(userData.passwordResetToken !== token)
            res.status(400).send({error: 'Invalid token'});
        else if(userData.passwordResetTokenExpiration < Date.now())
            res.status(400).send({error: 'Expired token'});
        
        userData.passwordResetToken = undefined,
        userData.passwordResetTokenExpiration = undefined,
        userData.password = newPassword;

        userData.save().then(()=>{
            res.send({message: 'Password changed successfully'});
        });
    }catch(e){
        console.error('Error', e);
        res.status(500).send('Internal server error');
    }
});

export default router;
