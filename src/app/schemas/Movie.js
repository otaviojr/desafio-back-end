import mongoose from '@/database';

const MovieSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String, 
        required: true
    },
    year: {
        type: Number,
        required: true
    },     
    createdAt: {
        type: Date,
        default: Date.now
    }
});

export default mongoose.model('Movie', MovieSchema);
