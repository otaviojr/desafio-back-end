import Slugify from 'slugify';

export default str => {
    return Slugify(str, {
        lower: true,
        remove: /[*+~.()'`"!:@]/g
    });
};