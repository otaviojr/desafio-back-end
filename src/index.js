import express from 'express';
import swaggerUi  from 'swagger-ui-express';
import { MovieController, AuthController } from '@/app/controllers';

import swaggerDocs from '../swagger.json';

const app = express();
const port = 3000;

app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use('/auth', AuthController);
app.use('/movie', MovieController);

app.listen(port, error => {
    if(error)
        console.error('Error starting server', error);
    else
        console.log(`Server started on port ${port}`);
});
