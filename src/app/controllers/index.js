import MovieController from './Movie';
import AuthController from './Auth';

export {MovieController, AuthController};
