import mongoose from 'mongoose';

mongoose.connect('mongodb://localhost/movies');

export default mongoose;